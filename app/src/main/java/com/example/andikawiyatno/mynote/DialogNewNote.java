package com.example.andikawiyatno.mynote;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by andikawiyatno on 1/9/18.
 */

public class DialogNewNote extends DialogFragment{
    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_new_note,null);

        final EditText edtTitle = (EditText)dialogView.findViewById(R.id.edt_title);
        final EditText edtDescription = (EditText)dialogView.findViewById(R.id.edt_description);
        final CheckBox checkBoxIdea = (CheckBox)dialogView.findViewById(R.id.cb_idea);
        final CheckBox checkBoxImportant = (CheckBox)dialogView.findViewById(R.id.cb_important);
        final CheckBox checkBoxTodo = (CheckBox)dialogView.findViewById(R.id.cb_todo);
        final Button btnCancel = (Button)dialogView.findViewById(R.id.btn_cancel);
        final Button btnOk = (Button)dialogView.findViewById(R.id.btn_ok);

        builder.setView(dialogView).setMessage("Tambah Catatan Baru");

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Note newNote = new Note();
                newNote.setTitle(edtTitle.getText().toString());
                newNote.setDescription((edtDescription.getText().toString()));
                //checkbox
                newNote.setIdea(checkBoxIdea.isChecked());
                newNote.setImportant(checkBoxImportant.isChecked());
                newNote.setTodo(checkBoxTodo.isChecked());

                MainActivity callingActivity = (MainActivity)getActivity();
                callingActivity.createNewNote(newNote);
                dismiss();
            }
        });

        return builder.create();


    }
}
