package com.example.andikawiyatno.mynote;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by andikawiyatno on 1/9/18.
 */

public class DialogShowNote extends DialogFragment {
    private Note mNote;

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_show_note,null);

        TextView txtTitle = (TextView)dialogView.findViewById(R.id.txt_title);
        TextView txtDescription = (TextView)dialogView.findViewById(R.id.txt_description);
        ImageView imgImportant = (ImageView)dialogView.findViewById(R.id.img_important);
        ImageView imgTodo = (ImageView)dialogView.findViewById(R.id.img_todo);
        ImageView imgIdea = (ImageView)dialogView.findViewById(R.id.img_idea);
        Button btnOk = (Button)dialogView.findViewById(R.id.btn_ok);

        //set value untuk di tampilkan
        if(!mNote.isIdea()){
            imgIdea.setVisibility(View.GONE);
        }
        if(!mNote.isImportant()){
            imgImportant.setVisibility(View.GONE);
        }
        if (!mNote.isTodo()){
            imgTodo.setVisibility(View.GONE);
        }

        txtTitle.setText(mNote.getTitle());
        txtDescription.setText(mNote.getDescription());
        builder.setView(dialogView).setMessage("Your Note");

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return builder.create();

    }

    public void sendNoteSelected(Note noteSelected){
        //kirim data
        mNote=noteSelected;
    }

}
